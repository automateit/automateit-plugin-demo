-keep class * extends com.smarterapps.automateitplugin.sdk.PluginAction
-keepclassmembers class * extends com.smarterapps.automateitplugin.sdk.PluginAction {
public <methods>;
}

-keep class * extends com.smarterapps.automateitplugin.sdk.PluginTrigger
-keepclassmembers class * extends com.smarterapps.automateitplugin.sdk.PluginTrigger {
public <methods>;
}

-keep class * extends com.smarterapps.automateitplugin.sdk.IPluginDataFieldCustomView
-keepclassmembers class * extends com.smarterapps.automateitplugin.sdk.IPluginDataFieldCustomView {
public <methods>;
}

-keepclassmembers class com.smarterapps.automateitplugin.sdk.PluginValidationResult {
public <methods>;
}