package com.smarterapps.demoplugin.actions;

import java.util.List;

import android.content.Context;
import android.widget.Toast;

import com.smarterapps.automateitplugin.sdk.PluginDataFieldCollection;
import com.smarterapps.automateitplugin.sdk.RequiredApp;
import com.smarterapps.automateitplugin.sdk.fields.PluginDataFieldCustom;
import com.smarterapps.automateitplugin.sdk.PluginValidationResult;
import com.smarterapps.demoplugin.EditTextRed;
import com.smarterapps.demoplugin.R;

public class Action2 extends com.smarterapps.automateitplugin.sdk.PluginAction
{
	private static final int FIELD_ID_FIRST_STRING = 200;
	private static final int FIELD_ID_SECOND_STRING = 201;

	@Override
	public String getActionTitle(Context context)
	{
		return context.getString(R.string.action2_title);
	}

	@Override
	public String getActionDescription(Context context)
	{
		return context.getString(R.string.action2_default_description);
	}

	@Override
	public String getActionDescription(Context context, PluginDataFieldCollection data)
	{
		String firstString = (String) data.getFieldById(FIELD_ID_FIRST_STRING).getValue();
		String secondString = (String) data.getFieldById(FIELD_ID_SECOND_STRING).getValue();

		if (firstString.length() == 0 && secondString.length() == 0)
		{
			return getActionDescription(context);
		}
		else if (firstString.length() == 0)
		{
			return context.getString(R.string.action2_description_partial, secondString);
		}
		else if (secondString.length() == 0)
		{
			return context.getString(R.string.action2_description_partial, firstString);
		}

		return context.getString(R.string.action2_description_full, firstString, secondString);
	}

	@Override
	public int getActionIconResourceId()
	{
		return R.drawable.ic_action2;
	}

	@Override
	public int getActionSmallIconResourceId()
	{
		return R.drawable.ic_action2_small;
	}

	@Override
	public PluginDataFieldCollection getActionFields(Context context)
	{
		PluginDataFieldCollection retval = new PluginDataFieldCollection();

		retval.add(new PluginDataFieldCustom(
				FIELD_ID_FIRST_STRING,
				context.getString(R.string.action2_field_first_string_title),
				context.getString(R.string.action2_field_first_string_description),
				EditTextRed.class));

		retval.add(new PluginDataFieldCustom(
				FIELD_ID_SECOND_STRING,
				context.getString(R.string.action2_field_second_string_title),
				context.getString(R.string.action2_field_second_string_description),
				EditTextRed.class));

		return retval;
	}

	@Override
	public void doAction(Context context, PluginDataFieldCollection data)
	{
		String firstString = (String) data.getFieldById(FIELD_ID_FIRST_STRING).getValue();
		String secondString = (String) data.getFieldById(FIELD_ID_SECOND_STRING).getValue();

		String toastMsg = "";
		if (firstString.length() == 0 && secondString.length() == 0)
		{
			toastMsg = context.getString(R.string.action2_toast_message_no_strings);
		}
		else if (firstString.length() == 0)
		{
			toastMsg = context.getString(R.string.action2_toast_message_partial, secondString);
		}
		else if (secondString.length() == 0)
		{
			toastMsg = context.getString(R.string.action2_toast_message_partial, firstString);
		}
		else
		{
			toastMsg = context.getString(R.string.action2_toast_message_full, firstString, secondString);
		}

		Toast.makeText(context, toastMsg, Toast.LENGTH_LONG).show();
	}

	@Override
	public PluginValidationResult validateData(Context context, PluginDataFieldCollection data)
	{
		// Validate action2
		String firstString = (String) data.getFieldById(FIELD_ID_FIRST_STRING).getValue();
		String secondString = (String) data.getFieldById(FIELD_ID_SECOND_STRING).getValue();

		if (null == firstString || firstString.trim().length() == 0)
		{
			return new PluginValidationResult(false, "Toast first String can't be empty");
		}
		
		if (null == secondString || secondString.trim().length() == 0)
		{
			return new PluginValidationResult(false, "Toast second String can't be empty");
		}

		return PluginValidationResult.getValidResult();
	}

	@Override
	public List<RequiredApp> getRequiredApps()
	{
		return null;
	}
}