package com.smarterapps.demoplugin.actions;

import java.util.List;

import android.content.Context;
import android.widget.Toast;

import com.smarterapps.automateitplugin.sdk.PluginDataFieldCollection;
import com.smarterapps.automateitplugin.sdk.PluginValidationResult;
import com.smarterapps.automateitplugin.sdk.RequiredApp;
import com.smarterapps.automateitplugin.sdk.fields.PluginDataFieldBoolean;
import com.smarterapps.automateitplugin.sdk.fields.PluginDataFieldString;
import com.smarterapps.demoplugin.R;

public class Action1 extends com.smarterapps.automateitplugin.sdk.PluginAction
{
	private static final int FIELD_ID_TEXT_TO_SHOW = 1;
	private static final int FIELD_ID_DURATION_LONG = 2;

	@Override
	public String getActionTitle(Context context)
	{
		return context.getString(R.string.action1_title);
	}

	@Override
	public String getActionDescription(Context context)
	{
		return context.getString(R.string.action1_default_description);
	}

	@Override
	public String getActionDescription(Context context, PluginDataFieldCollection data)
	{
		if (null != data)
		{
			String msgText = (String) data.getFieldById(FIELD_ID_TEXT_TO_SHOW).getValue();
			boolean durationLong = (Boolean) data.getFieldById(FIELD_ID_DURATION_LONG).getValue();

			int toastDurationStringId = durationLong ? R.string.duration_long: R.string.duration_short;

			return context.getString(R.string.action1_description,
					context.getString(toastDurationStringId),
					msgText);
		}

		return getActionDescription(context);
	}

	@Override
	public int getActionIconResourceId()
	{
		return R.drawable.ic_action1;
	}

	@Override
	public int getActionSmallIconResourceId()
	{
		return R.drawable.ic_action1_small;
	}

	@Override
	public PluginDataFieldCollection getActionFields(Context context)
	{
		PluginDataFieldCollection retval = new PluginDataFieldCollection();

		retval.add(new PluginDataFieldBoolean(
				FIELD_ID_DURATION_LONG, 
				context.getString(R.string.action1_field_duration_title), 
				context.getString(R.string.action1_field_duration_description),
				true));

		retval.add(new PluginDataFieldString(
				FIELD_ID_TEXT_TO_SHOW,
				context.getString(R.string.action1_field_msg_title),
				context.getString(R.string.action1_field_msg_description),
				""));

		return retval;
	}

	@Override
	public void doAction(Context context, PluginDataFieldCollection data)
	{
		boolean durationLong = (Boolean) data.getFieldById(FIELD_ID_DURATION_LONG).getValue();
		int toastDuration = durationLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;

		String toastMsg = (String) data.getFieldById(FIELD_ID_TEXT_TO_SHOW).getValue();
		Toast.makeText(context, toastMsg, toastDuration).show();
	}

	@Override
	public PluginValidationResult validateData(Context context, PluginDataFieldCollection data)
	{
		// Validate action1
		String toastMsg = (String) data.getFieldById(FIELD_ID_TEXT_TO_SHOW).getValue();

		if (null == toastMsg || toastMsg.trim().length() == 0)
		{
			return new PluginValidationResult(false, "Toast message can't be empty");
		}

		return PluginValidationResult.getValidResult();
	}

	@Override
	public List<RequiredApp> getRequiredApps()
	{
		return null;
	}
}