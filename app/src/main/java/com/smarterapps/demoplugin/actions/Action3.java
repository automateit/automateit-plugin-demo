package com.smarterapps.demoplugin.actions;

import java.util.List;

import android.content.Context;
import android.widget.Toast;

import com.smarterapps.automateitplugin.sdk.PluginDataFieldCollection;
import com.smarterapps.automateitplugin.sdk.PluginValidationResult;
import com.smarterapps.automateitplugin.sdk.RequiredApp;
import com.smarterapps.automateitplugin.sdk.fields.PluginDataFieldList;
import com.smarterapps.demoplugin.R;

public class Action3 extends com.smarterapps.automateitplugin.sdk.PluginAction
{
	private static final int FIELD_ID_SELECTED_MSG_INDEX = 301;

	@Override
	public String getActionTitle(Context context)
	{
		return context.getString(R.string.action3_title);
	}

	@Override
	public String getActionDescription(Context context)
	{
		return context.getString(R.string.action3_default_description);
	}

	@Override
	public String getActionDescription(Context context, PluginDataFieldCollection data)
	{
		PluginDataFieldList selectedMsgField = (PluginDataFieldList) data.getFieldById(FIELD_ID_SELECTED_MSG_INDEX);
		int selectedIndex = selectedMsgField.getSelectedIndex();
		String message = selectedMsgField.getItems()[selectedIndex];

		return context.getString(R.string.action3_description, message);
	}

	@Override
	public int getActionIconResourceId()
	{
		return R.drawable.ic_action3;
	}

	@Override
	public int getActionSmallIconResourceId()
	{
		return R.drawable.ic_action3_small;
	}

	@Override
	public PluginDataFieldCollection getActionFields(Context context)
	{
		PluginDataFieldCollection retval = new PluginDataFieldCollection();

		retval.add(new PluginDataFieldList(
				FIELD_ID_SELECTED_MSG_INDEX,
				context.getString(R.string.action3_field_title),
				context.getString(R.string.action3_field_description),
				new String[] {"Hi!", "Hello :-)", "Have a good day"},
				0));

		return retval;
	}

	@Override
	public void doAction(Context context, PluginDataFieldCollection data)
	{
		PluginDataFieldList selectedMsgField = (PluginDataFieldList) data.getFieldById(FIELD_ID_SELECTED_MSG_INDEX);
		int selectedIndex = selectedMsgField.getSelectedIndex();
		String message = selectedMsgField.getItems()[selectedIndex];
		
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	@Override
	public PluginValidationResult validateData(Context context, PluginDataFieldCollection data)
	{
		// Validate action3
		return PluginValidationResult.getValidResult();
	}

	@Override
	public List<RequiredApp> getRequiredApps()
	{
		return null;
	}
}