package com.smarterapps.demoplugin.actions;

import java.util.List;

import android.content.Context;
import android.widget.Toast;

import com.smarterapps.automateitplugin.sdk.PluginDataFieldCollection;
import com.smarterapps.automateitplugin.sdk.PluginValidationResult;
import com.smarterapps.automateitplugin.sdk.RequiredApp;
import com.smarterapps.automateitplugin.sdk.fields.PluginDataFieldCustom;
import com.smarterapps.demoplugin.ButtonSelectImage;
import com.smarterapps.demoplugin.R;

public class Action4 extends com.smarterapps.automateitplugin.sdk.PluginAction
{
	private static final int FIELD_ID_BUTTON = 401;

	@Override
	public String getActionTitle(Context context)
	{
		return context.getString(R.string.action4_title);
	}

	@Override
	public String getActionDescription(Context context)
	{
		return context.getString(R.string.action4_default_description);
	}

	@Override
	public String getActionDescription(Context context, PluginDataFieldCollection data)
	{
		String buttonText = (String) data.getFieldById(FIELD_ID_BUTTON).getValue();

		if (buttonText.length() == 0)
		{
			return getActionDescription(context);
		}

		return context.getString(R.string.action4_description_with_text, buttonText);
	}

	@Override
	public int getActionIconResourceId()
	{
		return R.drawable.ic_action4;
	}

	@Override
	public int getActionSmallIconResourceId()
	{
		return R.drawable.ic_action4_small;
	}

	@Override
	public PluginDataFieldCollection getActionFields(Context context)
	{
		PluginDataFieldCollection retval = new PluginDataFieldCollection();

		retval.add(new PluginDataFieldCustom(
				FIELD_ID_BUTTON,
				context.getString(R.string.action4_field_title),
				context.getString(R.string.action4_field_description),
				ButtonSelectImage.class));

		return retval;
	}

	@Override
	public void doAction(Context context, PluginDataFieldCollection data)
	{
		String buttonText = (String) data.getFieldById(FIELD_ID_BUTTON).getValue();

		Toast.makeText(context, buttonText, Toast.LENGTH_LONG).show();
	}

	@Override
	public PluginValidationResult validateData(Context context, PluginDataFieldCollection data)
	{
		// Validate action2
		String buttonText = (String) data.getFieldById(FIELD_ID_BUTTON).getValue();

		if (null == buttonText || buttonText.trim().length() == 0)
		{
			return new PluginValidationResult(false, "No image selected");
		}

		return PluginValidationResult.getValidResult();
	}

	@Override
	public List<RequiredApp> getRequiredApps()
	{
		return null;
	}
}