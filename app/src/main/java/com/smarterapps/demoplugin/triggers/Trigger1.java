package com.smarterapps.demoplugin.triggers;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.smarterapps.automateitplugin.sdk.PluginDataFieldCollection;
import com.smarterapps.automateitplugin.sdk.PluginValidationResult;
import com.smarterapps.automateitplugin.sdk.RequiredApp;
import com.smarterapps.demoplugin.R;

public class Trigger1 extends com.smarterapps.automateitplugin.sdk.PluginTrigger
{
	private static final String TAG = "AutomateItDemoPluginTrigger";

	private BroadcastReceiver m_receiver;

	@Override
	public String getTriggerTitle(Context context)
	{
		return "Screen ON Trigger";
	}

	@Override
	public String getTriggerDescription(Context context)
	{
		return "Launches when screen is turned on";
	}

	@Override
	public String getTriggerDescription(Context context, PluginDataFieldCollection data)
	{
		return getTriggerDescription(context);
	}

	@Override
	public int getTriggerIconResourceId()
	{
		return R.drawable.ic_trigger1;
	}
	
	@Override
	public int getTriggerSmallIconResourceId()
	{
		return R.drawable.ic_trigger1_small;
	}

	@Override
	public PluginDataFieldCollection getTriggerFields(Context context)
	{
		return null;
	}

	@Override
	public void startListening(Context context, PluginDataFieldCollection data)
	{
		Log.i(TAG, "Start listening...");

		// Usually, You should start a service and register the receiver on that service so it will
		// not get terminated by the OS, but to simplify the example, we are registering the receiver here

		m_receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				Log.i(TAG, "Trigger1 launched");

				launchTrigger();
			}
		};

		context.registerReceiver(m_receiver, new IntentFilter(Intent.ACTION_SCREEN_ON));
	}

	@Override
	public void stopListening(Context context)
	{
		Log.i(TAG, "Stop listening...");
		
		try
		{
			if (null != m_receiver)
			{
				context.unregisterReceiver(m_receiver);

				m_receiver = null;
			}
		}
		catch (Exception ex)
		{
			
		}
	}

	@Override
	public PluginValidationResult validateData(Context context, PluginDataFieldCollection data)
	{
		return PluginValidationResult.getValidResult();
	}

	@Override
	public List<RequiredApp> getRequiredApps()
	{
		return null;
	}
}