package com.smarterapps.demoplugin;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.smarterapps.automateitplugin.sdk.CustomViewUtils;
import com.smarterapps.automateitplugin.sdk.CustomViewUtils.IOnActivityResultCallback;
import com.smarterapps.automateitplugin.sdk.IPluginDataFieldCustomView;

public class ButtonSelectImage extends Button implements IPluginDataFieldCustomView, OnClickListener
{
	public ButtonSelectImage(Context context)
	{
		super(context);

		setOnClickListener(this);
	}

	@Override
	public String getFieldValue()
	{
		return getText().toString();
	}

	@Override
	public void setFieldValue(String value)
	{
		setText(value);
	}

	@Override
	public void onClick(View v)
	{
		Intent pickImageIntent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		CustomViewUtils.startActivityForResult(getContext(), pickImageIntent, 1, new IOnActivityResultCallback()
		{
			@Override
			public void onActivityResult(int requestCode, int resultCode, Intent data)
			{
				setText(data.getDataString());

				CustomViewUtils.refreshActionOrTriggerDescription(getContext());
			}
		});
	}
}