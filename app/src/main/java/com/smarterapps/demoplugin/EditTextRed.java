package com.smarterapps.demoplugin;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.smarterapps.automateitplugin.sdk.CustomViewUtils;
import com.smarterapps.automateitplugin.sdk.IPluginDataFieldCustomView;

public class EditTextRed extends EditText implements IPluginDataFieldCustomView
{
	public EditTextRed(Context context)
	{
		super(context);

		setTextColor(Color.RED);

		addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				// Do nothing
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
				// Do nothing
			}
			
			@Override
			public void afterTextChanged(Editable s)
			{
				CustomViewUtils.refreshActionOrTriggerDescription(getContext());
			}
		});
	}

	@Override
	public String getFieldValue()
	{
		return getText().toString();
	}

	@Override
	public void setFieldValue(String value)
	{
		setText(value);
	}
}