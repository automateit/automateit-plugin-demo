#AutomateIt Plugin Demo#

This repository is a demo plugin for [AutomateIt](https://play.google.com/store/apps/details?id=AutomateIt.mainPackage) app.

### How do I get set up? ###
To use the Plugin SDK and build your own plugin, insert the following dependency to your project build.gradle file:

```
#!groovy

compile 'com.automateitapp:automateit.plugin.sdk:1.0.3'
```

More details are available at the [AutomateIt Plugin Developer Guide](http://automateitapp.com/plugins/automateit-plugin-developer-guide/) on the app website